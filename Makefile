container_version = 0.0.1
container_name = nvim-portable

build:
	podman build -t $(container_name):$(container_version) -f ./container/nvim.Containerfile ./container

.PHONY: build
