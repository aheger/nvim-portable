# nvim-portable

## Project goals
- create a portable container with my preferred neovim setup

## Challenges
- Which base container to use?
- Which folders should be mounted by default?
- How to translate between host/guest paths?
- Where to store vim swap/undo?

## Requirements

- [podman](https://podman.io/)

## Usage

### Build the container

```
make build
```
